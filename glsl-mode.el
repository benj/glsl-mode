;;; glsl-mode.el --- major mode for GLSL

;; Author:  2010-2013 Ben Jones <benj2579 <at> gmail.com> starting from
;;                    http://cc-mode.sourceforge.net/derived-mode-ex.el
;; Version: 1.0

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'cc-mode)
(eval-when-compile
  (require 'cc-langs)
  (require 'cc-fonts))

(eval-and-compile
  (c-add-language 'glsl-mode 'c-mode))

(c-lang-defconst c-primitive-type-kwds
  glsl (append '("vec2" "vec3" "vec4" "ivec2" "ivec3" "ivec4"
		 "bvec2" "bvec3" "bvec4" "mat2" "mat3" "mat4"

		 ;; not in ES
		 "mat2x2" "mat2x3" "mat2x4"
		 "mat3x2" "mat3x3" "mat3x4"
		 "mat4x2" "mat4x3" "mat4x4"

		 "sampler2D" "samplerCube"

		 "void" "int" "uint" "float" "bool"
		 )
	       nil)
  )

(c-lang-defconst c-modifier-kwds
  glsl (append '("attribute" "const" "varying" "uniform"
		 "invariant"
		 "in" "out" "inout"
		 "input" "output"	; reserved in ES
		 "highp" "mediump" "lowp" "precision"
		 "superp"		; reserved in ES
		 "flat" "fixed"		; reserved in ES
		 "inline" "noinline"    ; reserved in ES
		 "external" "interface"	; reserved in ES
                 )
               nil)
  )

(c-lang-defconst c-simple-stmt-kwds
  glsl (append '("discard")
               (c-lang-const c-simple-stmt-kwds)
               nil)
  )

(c-lang-defconst comment-start
  glsl (c-lang-const comment-start c++))  ; // is allowed in GLSL

(defcustom glsl-font-lock-extra-types nil
  "*List of extra types (aside from the type keywords) to recognize in GLSL mode.
Each list item should be a regexp matching a single identifier.")

(defconst glsl-font-lock-keywords-1 (c-lang-const c-matchers-1 glsl)
  "Minimal highlighting for GLSL mode.")

(defconst glsl-font-lock-keywords-2 (c-lang-const c-matchers-2 glsl)
  "Fast normal highlighting for GLSL mode.")

(defconst glsl-font-lock-keywords-3 (c-lang-const c-matchers-3 glsl)
  "Accurate normal highlighting for GLSL mode.")

(defvar glsl-font-lock-keywords glsl-font-lock-keywords-3
  "Default expressions to highlight in GLSL mode.")

(defvar glsl-mode-syntax-table nil
  "Syntax table used in glsl-mode buffers.")
(or glsl-mode-syntax-table
    (setq glsl-mode-syntax-table
	  (funcall (c-lang-const c-make-mode-syntax-table glsl))))

(defvar glsl-mode-abbrev-table nil
  "Abbreviation table used in glsl-mode buffers.")
(c-define-abbrev-table 'glsl-mode-abbrev-table
  ;; Keywords that if they occur first on a line might alter the
  ;; syntactic context, and which therefore should trigger reindentation
  ;; when they are completed.
  '(("else" "else" c-electric-continued-statement 0)
    ("while" "while" c-electric-continued-statement 0)))

(defvar glsl-mode-map (let ((map (c-make-inherited-keymap)))
		      ;; Add bindings which are only useful for GLSL
			(define-key map (kbd "C-c C-v")
			  'glsl-switch-fragment/vertex-program)
			(define-key map (kbd "M-<up>") 'glsl-precision-up)
			(define-key map (kbd "M-<down>") 'glsl-precision-down)
			map)
  "Keymap used in glsl-mode buffers.")

;; Call the menu 'c-glsl-menu' because it makes a command by that name,
;; which otherwise gets in the way of M-x glsl-mode.
(easy-menu-define c-glsl-menu glsl-mode-map "GLSL Mode Commands"
  (cons "GLSL" (c-lang-const c-mode-menu glsl)))


(defun glsl-switch-fragment/vertex-program ()
  "Try to switch between matching fragment and vertex programs."
  (interactive)
  (let ((b (buffer-file-name)))
    (cond
     ((string-match "vertex" b)
      (find-file (replace-regexp-in-string "vertex" "fragment" b)))
     ((string-match "fragment" b)
      (find-file (replace-regexp-in-string "fragment" "vertex" b)))
     ((string-match "vert" b)
      (find-file (replace-regexp-in-string "vert" "frag" b)))
     ((string-match "frag" b)
      (find-file (replace-regexp-in-string "frag" "vert" b)))
     (t (error "Can't find 'frag' or 'vert' in file name"))
     ))
  )

(defun glsl-precision-change (direction)
  (save-excursion
    (let ((beg (progn (if (eq (point-at-bol) (point)) (point)
			(c-beginning-of-statement-1) (point))))
	  (end (progn (c-end-of-statement) (point))))
      (goto-char beg)
      (if (search-forward-regexp "\\(high\\|low\\|medium\\)p" end t)
	  (let ((precision (match-string 1))
		replacement)
	    (setq replacement
		  (cond
		   ((string= precision "low")
		    (cond ((eq direction 'up) "medium")
			  ((eq direction 'down) (error "No lower precision"))))
		   ((string= precision "medium")
		    (cond ((eq direction 'up) "high")
			  ((eq direction 'down) "low")))
		   ((string= precision "high")
		    (cond ((eq direction 'up) (error "No higher precision"))
			  ((eq direction 'down) "medium")))))
	    (delete-region (point) (progn (forward-word -1) (point)))
	    (insert (concat replacement "p"))
	    )
	(error "No precision specifier found")
	)
      ))
  )

(defun glsl-precision-up ()
  (interactive)
  (glsl-precision-change 'up)
  )

(defun glsl-precision-down ()
  (interactive)
  (glsl-precision-change 'down)
  )


;;;###autoload
(add-to-list 'auto-mode-alist '("\\.glsl\\'" . glsl-mode))

;;;###autoload
(defun glsl-mode ()
  "Major mode for editing GLSL shader code.

The hook `c-mode-common-hook' is run with no args at mode
initialization, then `glsl-mode-hook'.

Key bindings:
\\{glsl-mode-map}"
  (interactive)
  (kill-all-local-variables)
  (c-initialize-cc-mode t)
  (set-syntax-table glsl-mode-syntax-table)
  (setq major-mode 'glsl-mode
	mode-name "GLSL"
	local-abbrev-table glsl-mode-abbrev-table
	abbrev-mode t)
  (use-local-map glsl-mode-map)
  (c-init-language-vars glsl-mode)
  (c-common-init 'glsl-mode)
  (setq imenu-generic-expression cc-imenu-c-generic-expression)
  (easy-menu-add c-glsl-menu)
  (run-hooks 'c-mode-common-hook)
  (run-hooks 'glsl-mode-hook)
  (c-update-modeline))

(provide 'glsl-mode)

;;; glsl-mode.el ends here
